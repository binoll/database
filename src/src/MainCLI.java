package src;

import objects.CulturalObject;
import objects.Houses;
import objects.MetroStation;

import java.nio.file.*;
import java.util.ArrayList;
import java.util.Scanner;

public class MainCLI {
	private static String URL = "jdbc:sqlite:database/database.db";
	private static String PATH_DATA = "data/data.csv";
	private static String PATH_METRO = "data/metro.json";
	private static String PATH_HOUSES = "data/houses.csv";

	public static void main(String[] args) {
		createDatabaseDirectory();

		Database database = new Database(URL);
		ReaderJSON readerJSON = new ReaderJSON(PATH_METRO);
		ReaderCSV readerCSV = new ReaderCSV(PATH_DATA, PATH_HOUSES);

		Scanner scanner = new Scanner(System.in);
		int ch;

		while (true) {
			System.out.print("Choose number:\n" +
					"0. Import data\n" +
					"1. Calculation of statistics on the number of cultural objects for each metro station\n" +
					"2. Calculating statistics on the number of people per cultural object\n" +
					"3. Print sorted cultural objects\n" +
					"4. Print sorted metro stations\n" +
					"5. Print sorted houses\n" +
					"6. Filtration\n" +
					"7. Pagination\n" +
					"8. Exit\n");

			try {
				ch = Integer.parseInt(scanner.next());
			} catch (NumberFormatException e) {
				System.err.println("Error! This is not number: " + e.getMessage());
				continue;
			}

			switch (ch) {
				case 0: {
					if (database.isEmpty()) {
						readerJSON.importDb(database);
						readerCSV.importDb(database);
					}
					break;
				}
				case 1: {
					ArrayList<MetroStation> metroStations = database.metroStationsStatistic();

					for (var metroStation : metroStations) {
						System.out.println("Metro station: " + metroStation.getName() +
								", Count cultural objects: " + metroStation.getCountCulturalObjects());
					}
					break;
				}
				case 2: {
					ArrayList<CulturalObject> culturalObjects = database.culturalObjectsStatistic();

					for (var object : culturalObjects) {
						System.out.println("Cultural object: " + object.getName() +
								", Count houses: " + object.getCountHouses());
					}
					break;
				}
				case 3: {
					ArrayList<CulturalObject> culturalObjects = database.culturalObjectsSorting();

					for (var object : culturalObjects) {
						System.out.println(object);
					}
					break;
				}
				case 4: {
					ArrayList<MetroStation> metroStations = database.metroStationsSorting();

					for (var station : metroStations) {
						System.out.println(station);
					}
					break;
				}
				case 5: {
					ArrayList<Houses> houses = database.housesSorting();

					for (var house : houses) {
						System.out.println(house);
					}
					break;
				}
				case 6: {
					System.out.print("Write template: ");
					String str = scanner.next();
					ArrayList<MetroStation> listMetroStation = database.filter(str);

					if (listMetroStation.isEmpty()) {
						System.out.print("Result is empty!\n");
						continue;
					}

					System.out.print("\nResult:\n");

					for (var station : listMetroStation) {
						System.out.println(station);
					}
					break;
				}
				case 7: {
					int page = 0;

					while (true) {
						System.out.print("Write page (write -1 for exit): ");

						try {
							page = Integer.parseInt(scanner.next());
						} catch (NumberFormatException e) {
							System.err.println("Error! This is not number: " + e.getMessage());
							continue;
						}

						if (page == -1) {
							break;
						}

						ArrayList<CulturalObject> culturalObjects = database.pagination(page);

						System.out.println("Result:\n");

						for (var object : culturalObjects) {
							System.out.println(object);
						}
					}
					break;
				}
				case 8: {
					System.out.print("Goodbye!\n");
					database.close();
					return;
				}
				default: {
					System.out.print("Wrong number!\n");
				}
			}
		}
	}

	public static void createDatabaseDirectory() {
		String directoryPath = "database";

		try {
			Path path = Paths.get(directoryPath);

			if (!Files.exists(path)) {
				Files.createDirectories(path);
				System.out.println("Directory created successfully: " + path.toAbsolutePath());
			} else {
				System.out.println("Directory already exists: " + path.toAbsolutePath());
			}
		} catch (Exception e) {
			System.err.println("Error creating directory: " + e.getMessage());
		}
	}
}
