package src;

import java.net.URL;
import java.sql.*;

import java.util.ArrayList;

import objects.CulturalObject;
import objects.Houses;
import objects.MetroStation;

import static java.lang.Math.*;

public class Database implements AutoCloseable {
	private final Connection connection;
	private final Statement statement;

	private PreparedStatement createCulturalObjects;
	private PreparedStatement createMetroStations;
	private PreparedStatement createHouses;

	private PreparedStatement insertCulturalObjects;
	private PreparedStatement insertMetroStation;
	private PreparedStatement insertHouses;

	public Database(String url) {
		try {
			connection = DriverManager.getConnection(url);
			statement = connection.createStatement();

			createTables();
		} catch (SQLException e) {
			System.err.println("Error creating database connection: " + e.getMessage());
			throw new RuntimeException("Error creating database connection", e);
		}
	}

	private void createTables() {
		try {
			createCulturalObjects = connection.prepareStatement(
					"CREATE TABLE IF NOT EXISTS CulturalObjects " +
							"(Id INTEGER PRIMARY KEY AUTOINCREMENT, Name TEXT, Location TEXT, " +
							"Longitude DOUBLE, Latitude DOUBLE, CountHouses INTEGER)");
			createMetroStations = connection.prepareStatement(
					"CREATE TABLE IF NOT EXISTS MetroStations " +
							"(Id INTEGER PRIMARY KEY AUTOINCREMENT, Name TEXT, LineName TEXT, " +
							"Longitude DOUBLE, Latitude DOUBLE, CountCulturalObjects INTEGER)");
			createHouses = connection.prepareStatement("CREATE TABLE IF NOT EXISTS Houses " +
					"(Id INTEGER PRIMARY KEY AUTOINCREMENT, Address TEXT, Longitude DOUBLE, Latitude DOUBLE," +
					" Population INTEGER)");

			createCulturalObjects.execute();
			createMetroStations.execute();
			createHouses.execute();

			insertCulturalObjects = connection.prepareStatement(
					"INSERT INTO CulturalObjects " +
							"(Name, Location, Longitude, Latitude, CountHouses)" +
							" VALUES (?, ?, ?, ?, ?)");

			insertMetroStation = connection.prepareStatement(
					"INSERT INTO MetroStations " +
							"(Name, LineName, Longitude, Latitude, CountCulturalObjects) VALUES (?, ?, ?, ?, ?)");
			insertHouses = connection.prepareStatement(
					"INSERT INTO Houses " +
							"(Address, Longitude, Latitude, Population) VALUES (?, ?, ?, ?)");
		} catch (SQLException e) {
			System.err.println("Error initializing statements: " + e.getMessage());
			throw new RuntimeException("Error initializing statements", e);
		}
	}

	public void writeCulturalObjects(CulturalObject object) {
		try {
			insertCulturalObjects.setString(1, object.getName());
			insertCulturalObjects.setString(2, object.getLocation());
			insertCulturalObjects.setDouble(3, object.getLongitude());
			insertCulturalObjects.setDouble(4, object.getLatitude());
			insertCulturalObjects.setInt(5, object.getCountHouses());

			insertCulturalObjects.execute();
		} catch (SQLException e) {
			System.err.println("Error writing cultural object: " + e.getMessage());
			throw new RuntimeException("Error writing cultural object", e);
		}
	}

	public void writeMetroStations(MetroStation station) {
		try {
			insertMetroStation.setString(1, station.getName());
			insertMetroStation.setString(2, station.getLineName());
			insertMetroStation.setDouble(3, station.getLongitude());
			insertMetroStation.setDouble(4, station.getLatitude());
			insertMetroStation.setInt(5, station.getCountCulturalObjects());

			insertMetroStation.execute();
		} catch (SQLException e) {
			System.err.println("Error writing metro station: " + e.getMessage());
			throw new RuntimeException("Error writing metro station", e);
		}
	}

	public void writeHouses(Houses houses) {
		try {
			insertHouses.setString(1, houses.getAddress());
			insertHouses.setDouble(2, houses.getLongitude());
			insertHouses.setDouble(3, houses.getLatitude());
			insertHouses.setInt(4, houses.getPopulation());

			insertHouses.execute();
		} catch (SQLException e) {
			System.err.println("Error writing metro station: " + e.getMessage());
			throw new RuntimeException("Error writing metro station", e);
		}
	}

	public ArrayList<CulturalObject> readCulturalObjects() {
		ArrayList<CulturalObject> culturalObjects = new ArrayList<>();

		try {
			ResultSet resultSet = statement.executeQuery("SELECT * FROM CulturalObjects ORDER BY Name;");

			while (resultSet.next()) {
				CulturalObject object = new CulturalObject();
				object.setName(resultSet.getString("Name"));
				object.setLocation(resultSet.getString("Location"));
				object.setLongitude(resultSet.getDouble("Longitude"));
				object.setLatitude(resultSet.getDouble("Latitude"));
				object.setCountHouses(resultSet.getInt("CountHouses"));

				culturalObjects.add(object);
			}

		} catch (SQLException e) {
			System.err.println("Error reading cultural objects: " + e.getMessage());
		}

		return culturalObjects;
	}

	public ArrayList<MetroStation> readMetroStations() {
		ArrayList<MetroStation> metroStations = new ArrayList<>();

		try {
			ResultSet resultSet = statement.executeQuery("SELECT * FROM MetroStations ORDER BY Name;");

			while (resultSet.next()) {
				MetroStation station = new MetroStation();

				station.setName(resultSet.getString("Name"));
				station.setLineName(resultSet.getString("LineName"));
				station.setLongitude(resultSet.getDouble("Longitude"));
				station.setLatitude(resultSet.getDouble("Latitude"));
				station.setCountCulturalObjects(resultSet.getInt("CountCulturalObjects"));

				metroStations.add(station);
			}

		} catch (SQLException e) {
			System.err.println("Error reading metro stations: " + e.getMessage());
		}

		return metroStations;
	}

	public ArrayList<Houses> readHouses() {
		ArrayList<Houses> houses = new ArrayList<>();

		try {
			ResultSet resultSet = statement.executeQuery("SELECT * FROM Houses ORDER BY Address;");

			while (resultSet.next()) {
				Houses house = new Houses();
				house.setAddress(resultSet.getString("Address"));
				house.setLongitude(resultSet.getDouble("Longitude"));
				house.setLatitude(resultSet.getDouble("Latitude"));
				house.setPopulation(resultSet.getInt("Population"));

				houses.add(house);
			}
		} catch (SQLException e) {
			System.err.println("Error reading metro stations: " + e.getMessage());
		}

		return houses;
	}

	private ResultSet readDatabase(String query) {
		ResultSet resultSet = null;

		try {
			resultSet = statement.executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return resultSet;
	}

	private void updateMetroStations(String name, long count) {
		try {
			statement.executeUpdate("UPDATE MetroStations SET CountCulturalObjects=" +
					count + " WHERE Name=\"" + name + "\";");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void updateCulturalObjects(String name, long count) {
		try {
			statement.executeUpdate("UPDATE CulturalObjects SET CountHouses=" +
					count + " WHERE Name=\"" + name + "\";");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private double getDistance(double x1, double x2, double y1, double y2) {
		return sqrt((pow(x1 - x2, 2) + pow(y1 - y2, 2)));
	}

	public ArrayList<MetroStation> metroStationsStatistic() {
		ArrayList<MetroStation> metroStations = readMetroStations();
		ArrayList<CulturalObject> culturalObjects = readCulturalObjects();

		for (var station : metroStations) {
			double minDistance = station.getLongitude();
			long count = 0;

			for (var object : culturalObjects) {
				double distance = getDistance(
						station.getLatitude(),
						object.getLatitude(),
						station.getLongitude(),
						object.getLongitude()
				);

				if (distance <= minDistance) {
					minDistance = distance;
					++count;
				}
			}

			updateMetroStations(station.getName(), count);
		}

		return readMetroStations();
	}

	public ArrayList<CulturalObject> culturalObjectsStatistic() {
		ArrayList<CulturalObject> culturalObjects = readCulturalObjects();
		ArrayList<Houses> houses = readHouses();

		for (var object : culturalObjects) {
			double minDistance = object.getLongitude();
			long count = 0;

			for (var house : houses) {
				double distance = getDistance(
						house.getLatitude(),
						object.getLatitude(),
						house.getLongitude(),
						object.getLongitude()
				);

				if (distance <= minDistance) {
					minDistance = distance;
					count += house.getPopulation();
				}
			}

			updateCulturalObjects(object.getName(), count);
		}

		return readCulturalObjects();
	}

	public boolean isEmpty() {
		try {
			ResultSet resultSet = statement.executeQuery("SELECT 1 FROM CulturalObjects LIMIT 1");

			if (resultSet.next()) {
				return false;
			}

			return true;
		} catch (SQLException e) {
			System.err.println("Error checking if the database is empty: " + e.getMessage());
			throw new RuntimeException("Error checking if the database is empty", e);
		}
	}

	public ArrayList<CulturalObject> culturalObjectsSorting() {
		ArrayList<CulturalObject> culturalObjects = new ArrayList<>();

		try {
			ResultSet resultSet = statement.executeQuery("SELECT * FROM CulturalObjects ORDER BY Name;");

			while (resultSet.next()) {
				CulturalObject object = new CulturalObject();
				object.setName(resultSet.getString("Name"));
				object.setLocation(resultSet.getString("Location"));
				object.setLongitude(resultSet.getDouble("Longitude"));
				object.setLatitude(resultSet.getDouble("Latitude"));
				object.setCountHouses(resultSet.getInt("CountHouses"));

				culturalObjects.add(object);
			}

		} catch (SQLException e) {
			System.err.println("Error reading cultural objects: " + e.getMessage());
		}

		return culturalObjects;
	}

	public ArrayList<MetroStation> metroStationsSorting() {
		ArrayList<MetroStation> metroStations = new ArrayList<>();

		try {
			ResultSet resultSet = statement.executeQuery("SELECT * FROM MetroStations ORDER BY Name;");

			while (resultSet.next()) {
				MetroStation station = new MetroStation();
				station.setName(resultSet.getString("Name"));
				station.setLineName(resultSet.getString("LineName"));
				station.setLongitude(resultSet.getDouble("Longitude"));
				station.setLatitude(resultSet.getDouble("Latitude"));
				station.setCountCulturalObjects(resultSet.getInt("CountCulturalObjects"));

				metroStations.add(station);
			}

		} catch (SQLException e) {
			System.err.println("Error reading metro stations: " + e.getMessage());
		}

		return metroStations;
	}

	public ArrayList<Houses> housesSorting() {
		ArrayList<Houses> houses = new ArrayList<>();

		try {
			ResultSet resultSet = statement.executeQuery("SELECT * FROM Houses ORDER BY Address;");

			while (resultSet.next()) {
				Houses house = new Houses();
				house.setAddress(resultSet.getString("Address"));
				house.setLongitude(resultSet.getDouble("Longitude"));
				house.setLatitude(resultSet.getDouble("Latitude"));
				house.setPopulation(resultSet.getInt("Population"));

				houses.add(house);
			}
		} catch (SQLException e) {
			System.err.println("Error reading houses: " + e.getMessage());
		}

		return houses;
	}

	public ArrayList<MetroStation> filter(String str) {
		ArrayList<MetroStation> metroStations = new ArrayList<>();
		ResultSet resultSet;

		try {
			resultSet = statement.executeQuery("SELECT * FROM MetroStations WHERE Name LIKE \"" + str + "%\";");

			while (resultSet.next()) {
				MetroStation station = new MetroStation();

				station.setName(resultSet.getString("Name"));
				station.setLineName(resultSet.getString("LineName"));
				station.setLongitude(resultSet.getDouble("Longitude"));
				station.setLatitude(resultSet.getDouble("Latitude"));
				station.setCountCulturalObjects(resultSet.getInt("CountCulturalObjects"));

				metroStations.add(station);
			}
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}

		return metroStations;
	}

	public ArrayList<CulturalObject> pagination(int page) {
		ArrayList<CulturalObject> culturalObjects = new ArrayList<>();
		ResultSet resultSet = null;
		int limit = 10;

		try {
			resultSet = statement.executeQuery("SELECT * FROM CulturalObjects LIMIT " + limit +
					" OFFSET " + limit * page + ";");

			while (resultSet.next()) {
				CulturalObject object = new CulturalObject();
				object.setName(resultSet.getString("Name"));
				object.setLocation(resultSet.getString("Location"));
				object.setLongitude(resultSet.getDouble("Longitude"));
				object.setLatitude(resultSet.getDouble("Latitude"));
				object.setCountHouses(resultSet.getInt("CountHouses"));

				culturalObjects.add(object);
			}

		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}

		return culturalObjects;
	}

	@Override
	public void close() {
		try {
			if (connection != null) {
				connection.close();
			}
		} catch (SQLException e) {
			System.err.println("Error closing resources: " + e.getMessage());
		}
	}
}
