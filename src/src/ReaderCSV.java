package src;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import objects.CulturalObject;
import objects.Houses;

public class ReaderCSV {
	private final int DATA_COMMON_NAME = 1;
	private final int DATA_LOCATION = 16;
	private final int DATA_LONGITUDE = 22;
	private final int DATA_LATITUDE = 23;

	private final int HOUSES_POPULATION = 5;
	private final int HOUSES_ADDRESS = 8;
	private final int HOUSES_LONGITUDE = 2;
	private final int HOUSES_LATITUDE = 1;

	private final String fileData;
	private final String fileHouses;

	public ReaderCSV(String fileData, String fileHouses) {
		this.fileData = fileData;
		this.fileHouses = fileHouses;
	}

	public void importDb(Database db) {
		try (Reader readerData = new FileReader(fileData);
		     Reader readerHouses = new FileReader(fileHouses);
		     CSVParser parserData = new CSVParser(readerData, CSVFormat.DEFAULT.withHeader());
		     CSVParser parserHouses = new CSVParser(readerHouses, CSVFormat.DEFAULT.withHeader())) {

			for (CSVRecord csvRecord : parserData) {
				CulturalObject culturalObject = new CulturalObject();

				culturalObject.setName(getStringValue(csvRecord, DATA_COMMON_NAME));
				culturalObject.setLocation(getStringValue(csvRecord, DATA_LOCATION));
				culturalObject.setLongitude(getDoubleValue(csvRecord, DATA_LONGITUDE));
				culturalObject.setLatitude(getDoubleValue(csvRecord, DATA_LATITUDE));

				db.writeCulturalObjects(culturalObject);
			}

			for (CSVRecord csvRecord : parserHouses) {
				Houses houses = new Houses();

				houses.setAddress(getStringValue(csvRecord, HOUSES_ADDRESS));
				houses.setLongitude(getDoubleValue(csvRecord, HOUSES_LONGITUDE));
				houses.setLatitude(getDoubleValue(csvRecord, HOUSES_LATITUDE));
				houses.setPopulation(getIntValue(csvRecord, HOUSES_POPULATION));

				db.writeHouses(houses);
			}

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private String getStringValue(CSVRecord csvRecord, int columnIndex) {
		return csvRecord.get(columnIndex);
	}

	private double getDoubleValue(CSVRecord csvRecord, int columnIndex) {
		try {
			return Double.parseDouble(csvRecord.get(columnIndex));
		} catch (NumberFormatException e) {
			return 0.0;
		}
	}

	private int getIntValue(CSVRecord csvRecord, int columnIndex) {
		try {
			return Integer.parseInt(csvRecord.get(columnIndex));
		} catch (NumberFormatException e) {
			return 0;
		}
	}
}
