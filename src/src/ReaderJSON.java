package src;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import objects.MetroStation;

public class ReaderJSON {
	private String file;

	public ReaderJSON(String file) {
		this.file = file;
	}

	public void importDb(Database db) {
		String lineName, name;
		double longitude, latitude;
		JSONParser parser = new JSONParser();

		try (Reader reader = Files.newBufferedReader(Paths.get(file))) {
			Object obj = parser.parse(reader);
			JSONObject jsonObject = (JSONObject) obj;
			JSONArray lines = (JSONArray) jsonObject.get("lines");

			for (Object lineObj : lines) {
				JSONObject line = (JSONObject) lineObj;

				lineName = (String) line.get("name");
				JSONArray stations = (JSONArray) line.get("stations");

				for (Object stationObj : stations) {
					JSONObject station = (JSONObject) stationObj;

					name = (String) station.get("name");
					latitude = (Double) station.get("lat");
					longitude = (Double) station.get("lng");

					MetroStation metroStation = new MetroStation(name, lineName, longitude, latitude, 0);

					db.writeMetroStations(metroStation);
				}
			}
		} catch (IOException | ParseException e) {
			throw new RuntimeException("Error writing metro station", e);
		}
	}
}
