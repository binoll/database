package objects;

public class CulturalObject {
	private String name;
	private String location;
	private double longitude;
	private double latitude;
	private int countHouses;

	public CulturalObject() {
	}

	public CulturalObject(String Name, String location, double longitude, double latitude, int countHouses) {
		this.name = Name;
		this.location = location;
		this.longitude = longitude;
		this.latitude = latitude;
		this.countHouses = countHouses;
	}

	@Override
	public String toString() {
		return "CulturalObject{" +
				"commonName='" + name + '\'' +
				", location='" + location + '\'' +
				", longitude=" + longitude +
				", latitude=" + latitude +
				'}';
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name != null) {
			this.name = name;
		}
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		if (location != null) {
			this.location = location;
		}
	}

	public void setCountHouses(int countHouses) {
		this.countHouses = countHouses;
	}

	public int getCountHouses() {
		return countHouses;
	}
}
