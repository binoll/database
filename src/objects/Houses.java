package objects;

public class Houses {
	private String address;
	private double longitude;
	private double latitude;
	private int population;

	public Houses() {
	}

	public Houses(String address, double longitude, double latitude, int population) {
		this.address = address;
		this.longitude = longitude;
		this.latitude = latitude;
		this.population = population;
	}

	@Override
	public String toString() {
		return "Houses{" +
				"address='" + address + '\'' +
				", longitude=" + longitude +
				", latitude=" + latitude +
				'}';
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress() {
		return address;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setPopulation(int population) {
		this.population = population;
	}

	public int getPopulation() {
		return population;
	}
}
