package objects;

public class MetroStation {
	private String name;
	private String lineName;
	private double longitude;
	private double latitude;
	private int countCulturalObjects;

	public MetroStation() {
	}

	public MetroStation(String stationName, String lineName,
	                    double longitude, double latitude, int countCulturalObjects) {
		this.name = stationName;
		this.lineName = lineName;
		this.longitude = longitude;
		this.latitude = latitude;
		this.countCulturalObjects = countCulturalObjects;
	}

	@Override
	public String toString() {
		return "MetroStation{" +
				"name='" + name + '\'' +
				", lineName='" + lineName + '\'' +
				", longitude=" + longitude +
				", latitude=" + latitude +
				'}';
	}

	public void print() {
		System.out.println("Station: " + this.name + "\nLine: " + this.lineName +
				"\nLongitude: " + this.longitude + "\nLatitude: " + this.latitude);
	}

	public String getLineName() {
		return lineName;
	}

	public void setLineName(String lineName) {
		if (lineName != null) {
			this.lineName = lineName;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name != null) {
			this.name = name;
		}
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public void setCountCulturalObjects(int countCulturalObjects) {
		this.countCulturalObjects = countCulturalObjects;
	}

	public int getCountCulturalObjects() {
		return countCulturalObjects;
	}
}
