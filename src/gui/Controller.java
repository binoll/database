package gui;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import objects.CulturalObject;
import objects.Houses;
import objects.MetroStation;
import src.Database;
import src.ReaderCSV;
import src.ReaderJSON;

import static java.lang.Math.ceil;
import static java.lang.Math.floor;

public class Controller implements Initializable {
	private static Database database = null;
	private final int itemsPerPage = 20;
	private static String path = "database/database.db";
	private static String URL = "jdbc:sqlite:";
	private static String PATH_DATA = "data/data.csv";
	private static String PATH_METRO = "data/metro.json";
	private static String PATH_HOUSES = "data/houses.csv";

	@Override
	public void initialize(URL url, ResourceBundle resourceBundle) {
		culObjName.setCellValueFactory(new PropertyValueFactory<>("Name"));
		culObjLoc.setCellValueFactory(new PropertyValueFactory<>("Location"));
		culObjLon.setCellValueFactory(new PropertyValueFactory<>("Longitude"));
		culObjLat.setCellValueFactory(new PropertyValueFactory<>("Latitude"));

		metStatName.setCellValueFactory(new PropertyValueFactory<>("Name"));
		metStatLineName.setCellValueFactory(new PropertyValueFactory<>("LineName"));
		metStatLon.setCellValueFactory(new PropertyValueFactory<>("Longitude"));
		metStatLat.setCellValueFactory(new PropertyValueFactory<>("Latitude"));

		housesAddr.setCellValueFactory(new PropertyValueFactory<>("Address"));
		housesLon.setCellValueFactory(new PropertyValueFactory<>("Longitude"));
		housesLat.setCellValueFactory(new PropertyValueFactory<>("Latitude"));
		housesPop.setCellValueFactory(new PropertyValueFactory<>("Population"));

		culObjPagination.setVisible(false);
		metStatPagination.setVisible(false);
		housesPagination.setVisible(false);

		culObjStatistics.setVisible(false);
		metStatStatistics.setVisible(false);

		labelImport.setAlignment(Pos.CENTER);
		labelStatistics.setAlignment(Pos.CENTER);
		labelFilter.setAlignment(Pos.CENTER);
	}

	@FXML
	private TabPane tabPane;
	@FXML
	private Label databaseName;

	@FXML
	private Tab culObjTab;
	@FXML
	private TableView<CulturalObject> culObjTable;
	@FXML
	private TableColumn<CulturalObject, String> culObjName;
	@FXML
	private TableColumn<CulturalObject, String> culObjLoc;
	@FXML
	private TableColumn<CulturalObject, Double> culObjLon;
	@FXML
	private TableColumn<CulturalObject, Double> culObjLat;
	@FXML
	private Pagination culObjPagination;

	@FXML
	private Tab metStatTab;
	@FXML
	private TableView<MetroStation> metStatTable;
	@FXML
	private TableColumn<MetroStation, String> metStatName;
	@FXML
	private TableColumn<MetroStation, String> metStatLineName;
	@FXML
	private TableColumn<MetroStation, Double> metStatLon;
	@FXML
	private TableColumn<MetroStation, Double> metStatLat;
	@FXML
	private Pagination metStatPagination;

	@FXML
	private Tab housesTab;
	@FXML
	private TableView<Houses> housesTable;
	@FXML
	private TableColumn<Houses, String> housesAddr;
	@FXML
	private TableColumn<Houses, Double> housesLon;
	@FXML
	private TableColumn<Houses, Double> housesLat;
	@FXML
	private TableColumn<Houses, Integer> housesPop;
	@FXML
	private Pagination housesPagination;

	@FXML
	private AnchorPane importPane;
	@FXML
	private Label labelImport;
	@FXML
	private Button buttonImport;
	@FXML
	private TextField importField;

	@FXML
	private AnchorPane filterPane;
	@FXML
	private TextField filterField;
	@FXML
	private Label labelFilter;
	@FXML
	private Button buttonFilter;

	@FXML
	private AnchorPane statisticsPane;
	@FXML
	private Label labelStatistics;
	@FXML
	private TableColumn<CulturalObject, Integer> culObjStatistics;
	@FXML
	private TableColumn<CulturalObject, Integer> metStatStatistics;

	@FXML
	void importDatabase() {
		String path = importField.getText();
		File file = new File(path);

		if (path.isEmpty()) {
			try {
				database = new Database(URL + path);

				if (database.isEmpty()) {
					ReaderJSON readerJSON = new ReaderJSON(PATH_METRO);
					ReaderCSV readerCSV = new ReaderCSV(PATH_DATA, PATH_HOUSES);

					readerJSON.importDb(database);
					readerCSV.importDb(database);
				}
				reloadDatabase();

				culObjPagination.setVisible(true);
				metStatPagination.setVisible(true);
				housesPagination.setVisible(true);

				databaseName.setText(path);
				importPane.setVisible(true);
				labelImport.setStyle("-fx-text-fill: black;");
				labelImport.setAlignment(Pos.CENTER);
				importField.setVisible(false);
				labelImport.setText("Data were updated by default paths!");

				buttonImport.setVisible(false);
			} catch (Exception e) {
				importPane.setVisible(true);
				labelImport.setStyle("-fx-text-fill: red;");
				labelImport.setAlignment(Pos.CENTER);
				labelImport.setText("Error! Necessary files by default did not exist!");
				importField.setVisible(false);
				buttonImport.setVisible(false);
			}
			return;
		}

		if (!file.exists() || file.isDirectory()) {
			importPane.setVisible(true);
			labelImport.setStyle("-fx-text-fill: red;");
			labelImport.setAlignment(Pos.CENTER);
			labelImport.setText("Error! Your path is wrong!");
			importField.setVisible(false);
			buttonImport.setVisible(false);
			return;
		}

		try {
			String URL = this.URL + path;
			database = new Database(URL);
			reloadDatabase();
			culObjPagination.setVisible(true);
			metStatPagination.setVisible(true);
			housesPagination.setVisible(true);

			databaseName.setText(path);
			importPane.setVisible(true);
			labelImport.setStyle("-fx-text-fill: black;");
			labelImport.setAlignment(Pos.CENTER);
			importField.setVisible(false);
			labelImport.setText("Data were updated!");
			buttonImport.setVisible(false);
		} catch (Exception e) {
			System.err.println("Error! Database were broke!");
		}
	}

	@FXML
	void exit() {
		Stage primaryStage = (Stage) tabPane.getScene().getWindow();
		primaryStage.close();
	}

	@FXML
	void back() {
		importPane.setVisible(false);
		filterPane.setVisible(false);
		statisticsPane.setVisible(false);
	}

	@FXML
	void importData() {
		importPane.setVisible(true);
		labelImport.setStyle("-fx-text-fill: black;");
		labelImport.setAlignment(Pos.CENTER);
		labelImport.setText("Import database (clear field for import by default):");
		importField.setText("");
		importField.setVisible(true);
		buttonImport.setVisible(true);
	}

	@FXML
	void filterMetStat() {
		filterPane.setVisible(true);
		filterField.setText("");

		if (database == null || database.isEmpty()) {
			labelFilter.setStyle("-fx-text-fill: red;");
			labelFilter.setAlignment(Pos.CENTER);
			labelFilter.setText("Error! You did not import data!");
			filterField.setVisible(false);
			buttonFilter.setVisible(false);
		} else {
			labelFilter.setStyle("-fx-text-fill: black;");
			labelFilter.setAlignment(Pos.CENTER);
			labelFilter.setText("Write name of metro stations:");
			filterField.setVisible(true);
			buttonFilter.setVisible(true);
		}
	}

	@FXML
	void calculateStatistics() {
		if (database == null || database.isEmpty()) {
			statisticsPane.setVisible(true);
			return;
		}

		database.culturalObjectsStatistic();
		database.metroStationsStatistic();

		culObjStatistics.setCellValueFactory(new PropertyValueFactory<>("CountHouses"));
		metStatStatistics.setCellValueFactory(new PropertyValueFactory<>("CountCulturalObjects"));
		reloadDatabase();
		culObjStatistics.setVisible(true);
		metStatStatistics.setVisible(true);

		labelStatistics.setText("OK! Statistics were calculated!");
		labelStatistics.setStyle("-fx-text-fill: black;");
		labelStatistics.setAlignment(Pos.CENTER);
		statisticsPane.setVisible(true);
	}

	@FXML
	public void filter() {
		ArrayList<MetroStation> list;
		SingleSelectionModel<Tab> selectionModel = tabPane.getSelectionModel();

		String text = filterField.getText();

		if (text.isEmpty()) {
			return;
		}

		list = database.filter(text);

		if (list == null || list.isEmpty()) {
			labelFilter.setStyle("-fx-text-fill: red;");
			labelFilter.setAlignment(Pos.CENTER);
			labelFilter.setText("Not found!");
			filterField.setVisible(false);
			buttonFilter.setVisible(false);
			return;
		}

		selectionModel.select(metStatTab);

		metStatPagination.setPageFactory(pageIndex -> {
			int fromIndex = pageIndex * itemsPerPage;
			int toIndex = fromIndex + itemsPerPage;
			int maxIndex = (int) floor((double) list.size() / itemsPerPage);

			if (metStatPagination.getCurrentPageIndex() < maxIndex) {
				metStatTable.setItems(FXCollections.observableArrayList(list.subList(fromIndex, toIndex)));
			} else {
				metStatTable.setItems(FXCollections.observableArrayList(list.subList(fromIndex, fromIndex + (list.size() % itemsPerPage))));
			}
			return metStatTable;
		});
		metStatPagination.setPageCount((int) ceil((double) list.size() / itemsPerPage));
		back();
	}

	@FXML
	void reviewData() {
		File file;
		FileChooser fileChooser;

		try {
			fileChooser = new FileChooser();
			file = fileChooser.showOpenDialog(new Stage());

			if (file == null || !file.exists() || file.isDirectory()) {
				importPane.setVisible(true);
				labelImport.setStyle("-fx-text-fill: red;");
				labelImport.setAlignment(Pos.CENTER);

				if (file == null) {
					labelImport.setText("Error! You did not choice file!");
				} else {
					labelImport.setText("Error! Your path is wrong!");
				}

				importField.setVisible(false);
				buttonImport.setVisible(false);
				return;
			}

			database = new Database(URL + file.getPath());
		} catch (Exception e) {
			importPane.setVisible(true);
			importField.setVisible(false);
			labelImport.setStyle("-fx-text-fill: red;");
			labelImport.setAlignment(Pos.CENTER);
			labelImport.setText("Error! The default directory is empty!");
			buttonImport.setVisible(false);
			return;
		}

		reloadDatabase();
		culObjPagination.setVisible(true);
		metStatPagination.setVisible(true);
		housesPagination.setVisible(true);

		databaseName.setText(file.getName());
		importPane.setVisible(true);
		labelImport.setStyle("-fx-text-fill: black;");
		labelImport.setAlignment(Pos.CENTER);
		importField.setVisible(false);
		labelImport.setText("Data were updated!");
		buttonImport.setVisible(false);
	}

	@FXML
	void reloadDatabase() {
		if (database == null || database.isEmpty()) {
			return;
		}

		String selectedTab = tabPane.getSelectionModel().getSelectedItem().getId();

		switch (selectedTab) {
			case "culObjTab": {
				ArrayList<CulturalObject> list = database.readCulturalObjects();

				culObjPagination.setPageFactory(pageIndex -> {
					int fromIndex = pageIndex * itemsPerPage;
					int toIndex = fromIndex + itemsPerPage;
					int maxIndex = (int) floor((double) list.size() / itemsPerPage);

					if (culObjPagination.getCurrentPageIndex() < maxIndex) {
						culObjTable.setItems(FXCollections.observableArrayList(list.subList(fromIndex, toIndex)));
					} else {
						culObjTable.setItems(FXCollections.observableArrayList(list.subList(fromIndex, fromIndex + (list.size() % itemsPerPage))));
					}
					return culObjTable;
				});
				culObjPagination.setPageCount((int) ceil((double) list.size() / itemsPerPage));
				break;
			}
			case "metStatTab": {
				ArrayList<MetroStation> list = database.readMetroStations();

				metStatPagination.setPageFactory(pageIndex -> {
					int fromIndex = pageIndex * itemsPerPage;
					int toIndex = fromIndex + itemsPerPage;
					int maxIndex = (int) floor((double) list.size() / itemsPerPage);

					if (metStatPagination.getCurrentPageIndex() < maxIndex) {
						metStatTable.setItems(FXCollections.observableArrayList(list.subList(fromIndex, toIndex)));
					} else {
						metStatTable.setItems(FXCollections.observableArrayList(list.subList(fromIndex, fromIndex + (list.size() % itemsPerPage))));
					}
					return metStatTable;
				});
				metStatPagination.setPageCount((int) ceil((double) list.size() / itemsPerPage));
				break;
			}
			case "housesTab": {
				ArrayList<Houses> list = database.readHouses();

				housesPagination.setPageFactory(pageIndex -> {
					int fromIndex = pageIndex * itemsPerPage;
					int toIndex = fromIndex + itemsPerPage;
					int maxIndex = (int) floor((double) list.size() / itemsPerPage);

					if (housesPagination.getCurrentPageIndex() < maxIndex) {
						housesTable.setItems(FXCollections.observableArrayList(list.subList(fromIndex, toIndex)));
					} else {
						housesTable.setItems(FXCollections.observableArrayList(list.subList(fromIndex, fromIndex + (list.size() % itemsPerPage))));
					}
					return housesTable;
				});
				housesPagination.setPageCount((int) ceil((double) list.size() / itemsPerPage));
				break;
			}
		}
	}
}
